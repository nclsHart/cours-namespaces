<?php

namespace MyNamespace;

use League\CLImate\CLImate;
use MyNamespace\Sub\MySubClass;

require_once __DIR__ . '/vendor/autoload.php';

$class = [
    explode('\\', (new MyClass())->getClassName()),
    explode('\\', (new \MyNamespace\Sub\MyClass())->getClassName()),
    explode('\\', (new MySubClass())->getClassName())
];

$climate = new CLImate();
$climate->columns($class);