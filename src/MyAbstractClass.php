<?php

namespace MyNamespace;

abstract class MyAbstractClass
{
    public function getClassName()
    {
        return get_called_class();
    }
}
