# Cours namespace

## Énoncé

Vous devez créer les éléments suivants : 

* 3 classes qui étendent de la classe abstraite `MyAbstractClass` et pour chacune de ces classes appeler la méthode `getClassName`
* 1 script `console.php` placé à la racine du projet

Une fois exécuté, le script `console.php` devra afficher le résultat suivant : 

```shell
$ php console.php
MyNamespace     MyClass
MyNamespace     Sub         MyClass
MyNamespace     Sub         MySubClass
```

## Astuce

1. N'oubliez pas de configurer PSR-4 dans votre fichier `composer.json`
2. Afin d'obtenir facilement le visuel attendu, utilisez la méthode `columns` de la bibliothèque `league/climate`.
Celle-ci devra être installée avec Composer.

